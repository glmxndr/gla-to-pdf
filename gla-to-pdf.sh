#! /bin/bash

BASE=https://graphicallinearalgebra.net

PROXY="-p ${http_proxy}"
FORMAT="--page-height 210mm --page-width 157mm -B 2mm -T 10mm -L 2mm -R 2mm "
#FORMAT="--page-height 210mm --page-width 157mm -O Landscape -B 5mm -L 5mm -T 5mm -R 80mm "
OUTLINE="--outline --outline-depth 1 "
STYLE="--user-style-sheet ./style.css "
JAVASCRIPT="--disable-javascript "
DEST=gla.pdf

set -x

wkhtmltopdf $PROXY $FORMAT $OUTLINE $STYLE $JAVASCRIPT \
    --header-left "[section]" --header-right "[page]" --header-line --header-font-name "URW Gothic L" --header-font-size 8 \
    cover ./title.html \
    cover ./intro.html \
    page $BASE/2015/04/23/makelele-and-linear-algebra/  \
    page $BASE/2015/04/24/methodology-handwaving-and-diagrams/ \
    cover ./addcopy.html \
    page $BASE/2015/04/26/adding-part-1-and-mr-fibonacci/  \
    page $BASE/2015/04/29/dumbing-down-magic-lego-and-the-rules-of-the-game-part-1/  \
    page $BASE/2015/04/30/spoilers-adding-part-2-and-zero/  \
    page $BASE/2015/05/06/crema-di-mascarpone-rules-of-the-game-part-2-and-diagrammatic-reasoning/  \
    page $BASE/2015/05/09/copying-discarding-and-the-slogan/  \
    page $BASE/2015/05/12/when-adding-met-copying/  \
    page $BASE/2015/05/16/natural-numbers-diagrammatically/  \
    cover ./matricesprops.html \
    page $BASE/2015/05/19/paths-and-matrices-part-1/ \
    page $BASE/2015/05/23/from-diagrams-to-matrices/ \
    page $BASE/2015/05/26/monoidal-categories-and-props-part-1/ \
    page $BASE/2015/06/01/props-part-2-and-permutations/ \
    page $BASE/2015/06/05/homomorphisms-of-props/ \
    page $BASE/2015/06/09/matrices-diagrammatically/ \
    page $BASE/2015/06/13/trust-the-homomorphism-for-it-is-fully-faithful/ \
    cover ./intsrels.html \
    page $BASE/2015/06/16/maths-with-diagrams/ \
    page $BASE/2015/06/30/introducing-the-antipode/ \
    page $BASE/2015/07/08/integer-matrices/ \
    page $BASE/2015/08/04/causality-feedback-and-relations/ \
    page $BASE/2015/09/08/21-functions-and-relations-diagrammatically/ \
    page $BASE/2015/10/01/22-the-frobenius-equation/ \
    page $BASE/2015/10/21/23-frobenius-snakes-and-spiders/ \
    cover ./fracspaces.html \
    page $BASE/2015/11/18/24-bringing-it-all-together/ \
    page $BASE/2015/11/24/25-fractions-diagrammatically/ \
    page $BASE/2015/12/14/26-keep-calm-and-divide-by-zero/ \
    page $BASE/2015/12/26/27-linear-relations/ \
    page $BASE/2016/02/03/28-subspaces-diagrammatically/ \
    page $BASE/2016/06/22/29-inverting-matrices-and-dividing-by-zero/ \
    page $BASE/2016/08/26/30-the-essence-of-graphical-linear-algebra/ \
    cover ./redundancy.html \
    page $BASE/2017/10/23/episode-r1-redundancy-and-zebra-snakes/ \
    cover ./stringdiagrams.html \
    page $BASE/2017/04/24/why-string-diagrams/ \
    cover ./sequencessfg.html \
    page $BASE/2016/09/07/31-fibonacci-and-sustainable-rabbit-farming/ \
    cover ./outororder.html \
    page $BASE/2017/08/09/orthogonality-and-projections/ \
    page $BASE/2017/09/12/eigenstuff-diagrammatically/ \
    cover ./contribs.html \
    page $BASE/2017/07/31/determinants-and-the-lindstrom-gessel-vienot-lemma/ \
    $DEST
